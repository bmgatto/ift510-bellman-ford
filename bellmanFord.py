#!/usr/bin/python3

def pathsInit():
    paths = {
        'u':{ 'v':3, 'w':7, 'x':1 }, 
        'v':{ 'u':3, 'w':2, 'x':4 },
        'w':{ 'u':7, 'v':2, 'x':8, 'y':2, 'z':2 },
        'x':{ 'u':1, 'v':4, 'w':8, 'y':3 },
        'y':{ 'w':2, 'x':3, 'z':5 },
        'z':{ 'y':5, 'w':2 },
        }
    return paths

def nodePrinter(currentPath):
    order = ['u','v','w','x','y','z']
    print("Routing Table: ",end='')
    for i in order:
        print(str(i) + ":" + str(currentPath[i]) + "  ", end='')
    print()

def hopPrinter(currentPath):
    order = ['u','v','w','x','y','z']
    print("Hops:          ", end='')
    for i in order:
        print(str(i) + ":" + str(currentPath[i]) + "  ", end='')
    print("\n")

def bellford(startNode):
    infinity = 25856
    nodesToCheck = paths
    nodeCost = {}
    nodeHops = {}
    lastNode = {}

    for key in nodesToCheck:
        nodeCost[key] = infinity
        nodeHops[key] = 0

    print("The starting node must find itself")
    nodePrinter(nodeCost)
    nodeCost[startNode] = 0
    nodePrinter(nodeCost)
    
    tick = 0
    while nodesToCheck:
        tick += 1
        print("\nIteration " + str(tick))
        smallestNode = None
        for node in nodesToCheck:
            if smallestNode is None:
                smallestNode = node
            elif nodeCost[node] < nodeCost[smallestNode]:
                smallestNode = node 

        for childNode, cost in paths[smallestNode].items():
            if cost + nodeCost[smallestNode] < nodeCost[childNode]:
                nodeCost[childNode] = cost + nodeCost[smallestNode]
                nodeHops[childNode] += 1
                lastNode[childNode] = smallestNode

            nodePrinter(nodeCost)
            hopPrinter(nodeHops)
        nodesToCheck.pop(smallestNode)
 
    

###########################################################

paths = pathsInit()
bellford('y')
